<?php
namespace Drupal\console_log\Controller;


use Drupal\Core\Controller\ControllerBase;

/**
 * Console log controller.
 */
class ConsoleLog extends ControllerBase {

  const PRIORITY_NORMAL = 'normal';

  public function log($message, $priority = self::PRIORITY_NORMAL) {

  }
}
