<?php

namespace Drupal\console_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the console log module.
 */
class ConsoleLogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'console_log.colors',
      'console_log.method',
      'console_log.active'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'console_log_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('console_log.settings')->get();

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => t('Console log module is active (For any user)'),
      '#description' => t('This option turns on module for <b>ALL USERS</b> independent from the permissions given inside the Drupal permissions. Please do not enable it on production.'),
      '#default_value' => !empty($config['active']['all']),
    ];
    $form['active_by_permission'] = [
      '#type' => 'checkbox',
      '#title' => t('Console log module is active (By permission)'),
      '#default_value' => !empty($config['active']['by_permission']),
    ];

    $form['options'] = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      // @todo restore states functionality.
      /*      '#states' => [
        'invisible' => [
          // One of the options must be active to show others.
          [':input[name="console_log_is_active"]' => ['checked' => TRUE]],
          'OR',
          [':input[name="console_log_is_active_by_permission"]' => ['checked' => TRUE]],
        ],
      ],*/
    ];


    $form['options']['priority'] = [
      '#title' => t('Priority colors'),
      '#type' => 'fieldset',
    ];

    $form['options']['priority']['high'] = [
      '#type' => 'color',
      '#title' => t('High'),
      '#default_value' => !empty($config['colors']['high']) ? $config['colors']['high'] : '#770209',
    ];

    $form['options']['priority']['normal'] = [
      '#type' => 'color',
      '#title' => t('Normal'),
      '#default_value' => !empty($config['colors']['normal']) ? $config['colors']['normal'] : '#770209',
    ];

    $form['options']['method'] = [
      '#type' => 'select',
      '#title' => t('Javascript method'),
      '#default_value' => !empty($config['method']) ? $config['colors'] : 'info',
      '#options' => [
        'log' => 'console.log',
        'info' => 'console.info',
        'warn' => 'console.warn',
        'error' => 'console.error',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = [
      'active' => [
        'all' => $form_state->getValue('active'),
        'by_permission' => $form_state->getValue('active_by_permission'),
      ],
      'colors' => [
        'high' => $form_state->getValue('high'),
        'normal' => $form_state->getValue('normal'),
      ],
      'method' => $form_state->getValue('method'),
    ];

    $this
      ->configFactory()
      ->getEditable('console_log.settings')
      ->setData($config)
      ->save();

    parent::submitForm($form, $form_state);
  }

}